package com.example.stockitems.model

data class StocksViewModel(
    val id: String,
    val name: String,
    val value: Int
)
