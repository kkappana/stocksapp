package com.example.stockitems.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.stockitems.R
import com.example.stockitems.model.StocksViewModel
import com.example.stockitems.service.StockService

class StockActivity : AppCompatActivity() {
    var existingData: ArrayList<StocksViewModel> = ArrayList(5)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stock)

        var recyclerView = findViewById<RecyclerView>(R.id.recyclerView_main)
        recyclerView.layoutManager = LinearLayoutManager(this)

        var buttonView = findViewById<Button>(R.id.btnRefresh)
        buttonView.setOnClickListener() {
            updateData()
        }
        val serviceConnector = StockService(recyclerView, this)
        serviceConnector.stocksDataResponse()
    }

    fun updateData() {
        var recyclerView = findViewById<RecyclerView>(R.id.recyclerView_main)
        val serviceConnector = StockService(recyclerView, this)
        serviceConnector.stocksDataResponse()
    }

    fun setPrevData(data: ArrayList<StocksViewModel>, position: Int) {
        if (existingData.size === data.size) {
            existingData[position] = data[position]
        } else {
            existingData.add(data[position])
        }
    }

    fun getPrevData(): ArrayList<StocksViewModel> {
        return existingData
    }
}