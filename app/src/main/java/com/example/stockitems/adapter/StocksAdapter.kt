package com.example.stockitems.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.stockitems.R
import com.example.stockitems.model.StocksViewModel
import com.example.stockitems.view.StockActivity

class StocksAdapter(
    private val mList: ArrayList<StocksViewModel>,
    private val stockActivity: StockActivity
) : RecyclerView.Adapter<StocksAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val stocksList = mList[position]
        var prevData = stockActivity.getPrevData()
        if (prevData.size === mList.size) {
            var prevStockData = prevData[position]
            if (stocksList.value > prevStockData.value) {
                holder.value.setTextColor(ContextCompat.getColor(stockActivity, R.color.green))
            } else if (stocksList.value < prevStockData.value) {
                holder.value.setTextColor(ContextCompat.getColor(stockActivity, R.color.red))
            } else if (stocksList.value == prevStockData.value) {
                holder.value.setTextColor(ContextCompat.getColor(stockActivity, R.color.black))
            }
        }
        holder.name.text = stocksList.name
        holder.value.text = stocksList.value.toString()
        stockActivity.setPrevData(mList, position)
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val value: TextView = itemView.findViewById(R.id.price)
    }

    override fun getItemCount(): Int {
        return mList.size
    }


}