package com.example.stockitems.service

data class StockResponseWrapper(
    val id: String,
    val name: String,
    val value: Int
)
