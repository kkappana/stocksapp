package com.example.stockitems.service

import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.RecyclerView
import com.example.stockitems.adapter.StocksAdapter
import com.example.stockitems.model.StocksViewModel
import com.example.stockitems.view.StockActivity
import retrofit2.Response
import java.util.concurrent.Executors

class StockService(
    private val recyclerView: RecyclerView,
    private val stockView: StockActivity
) {
    fun stocksDataResponse() {
        val stocksList = ArrayList<StocksViewModel>()
        val executor = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        executor.execute {
            val response: Response<List<StockResponseWrapper>> = ServiceCall.instance
                .getStockItems()
                .execute()
            handler.post {
                if (response.body()!!.isNotEmpty()) {
                    var dataList = response.body()!!
                    dataList.indices.forEach { it ->
                        stocksList.add(
                            StocksViewModel(
                                dataList[it].id,
                                dataList[it].name,
                                dataList[it].value
                            )
                        )
                    }
                    var sortedList =
                        stocksList.sortedWith(compareByDescending({ it.value })).subList(0, 5)

                    val myRecyclerViewAdapter =
                        StocksAdapter((ArrayList(sortedList)), stockView)
                    recyclerView.adapter = myRecyclerViewAdapter


                }
            }

        }

    }
}