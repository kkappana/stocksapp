package com.example.stockitems.service

import com.example.stockitems.BuildConfig
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ServiceCall {
    @GET("getStockList")
    fun getStockItems(): Call<List<StockResponseWrapper>>

    companion object {
        private val retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()
        }
        val instance: ServiceCall by lazy { retrofit.create(ServiceCall::class.java) }
    }

}