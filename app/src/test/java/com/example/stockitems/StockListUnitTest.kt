package com.example.stockitems

import com.example.stockitems.service.ServiceCall
import org.junit.Assert
import org.junit.Test

class StockListUnitTest {
@Test
    fun test_StockResponseCode(){
        val response = ServiceCall.instance
            .getStockItems()
            .execute()

        val errorBody = response.errorBody()
        Assert.assertNull("Received an error: ${errorBody?.string()}", errorBody)

        val responseWrapper = response.body()
        Assert.assertNotNull("Response is null.", responseWrapper)
        Assert.assertEquals("Response code", 200, response.code())
    }
}